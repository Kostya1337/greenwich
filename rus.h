#ifndef H_RUS
#define H_RUS

#include <string>
#include <sstream>
#include <iostream>

using namespace std;

// класс переводит в пропись трехзначные целые числа
// и подставляет соответствующие наименования степеней тысячи
class RusNum
{
private:
    static string hunds[];	// пропись сотен
    static string tens[];	// пропись десятков
    
public:
    static string Str(int val, bool thousand, string name);	// получаем пропись трехзначного числа
    static string Case(int val, bool thousand, string name);	// получаем правильное склонение наименования степени
};

// класс получает на вход динные (до 100 знаков) числа (в виде строки цифр)
// преобразует системы счисления и собирает представления числа прописью
class Rus
{
private:
    static string names[];						// наименования степеней тысячи
    static string DecToOct(string val);	// преобразование в 8сс
public:
    static void NumToStr();							// основная функция (ввод числа и вывод в 8сс и прописей)
    static string Str(string val);		// перевод числа в пропись
    
};

#endifKonstantin
