#include <vector>
#include <iostream>
#include <sstream>

#include "rus.h"

using namespace std;

string RusNum::hunds[] = { "", "сто ", "двести ", "триста ", "четыреста ",
    "пятьсот ", "шестьсот ", "семьсот ", "восемьсот ", "девятьсот " };

string RusNum::tens[] = { "", "десять ", "двадцать ", "тридцать ", "сорок ", "пятьдесят ",
    "шестьдесят ", "семьдесят ", "восемьдесят ", "девяносто " };

string RusNum::Str(int val, bool thousand, string name)
{
    // Числа до 10
    string frac20[] =
    {
        "", "один ", "два ", "три ", "четыре ", "пять ", "шесть ",
        "семь ", "восемь ", "девять ", "десять ", "одиннадцать ",
        "двенадцать ", "тринадцать ", "четырнадцать ", "пятнадцать ",
        "шестнадцать ", "семнадцать ", "восемнадцать ", "девятнадцать "
    };
    
    int num = val % 1000;
    if (0 == num) return "";
    if (num < 0) return "";
    // Только "тысяча" имеет женский род
    if (thousand)
    {
        frac20[1] = "одна ";
        frac20[2] = "две ";
    }
    
    stringstream r;
    r << (hunds[num / 100]);
    
    if (num % 100 < 20)
    {
        r << (frac20[num % 100]);
    }
    else
    {
        r << (tens[num % 100 / 10]);
        r << (frac20[num % 10]);
    }
    
    // Подбираем правильное наименование степени
    r << (Case(num, thousand, name));
    
    if (r.str().length() != 0) r << (" ");
    return r.str();
}


string RusNum::Case(int val, bool thousand, string name)
{
    int t = (val % 100 > 20) ? val % 10 : val % 20;
    
    if (thousand)
    {
        switch (t)
        {
            case 1: return name.append("а");	// тысяча
            case 2:
            case 3:
            case 4: return name.append("и");	// тысячи
            default: return name;
        }
    }
    else
    {
        if (name != "")
        {
            switch (t)
            {
                case 1: return name;	// миллион
                case 2:
                case 3:
                case 4: return name.append("а");	// миллиона
                default: return name.append("ов");	// миллионов
            }
        }
        else
            return name;
    }
    
    
}

string Rus::names[] = {
    "",
    "тысяч",
    "миллион",
    "миллиард",
    "биллион",
    "триллион",
    "квадриллион",
    "квинтиллион",
    "секстиллион",
    "септиллион",
    "октиллион",
    "нониллион",
    "дециллион",
    "ундециллион",
    "додециллион",
    "тредециллион",
    "кваттуордециллион",
    "квиндециллион",
    "седециллион",
    "септдециллион",
    "октодециллион",
    "новемдециллион",
    "вигинтиллион",
    "анвигинтиллион",
    "дуовигинтиллион",
    "тревигинтиллион",
    "кватторвигинтиллион",
    "квинвигинтиллион",
    "сексвигинтиллион",
    "септемвигинтиллион",
    "октовигинтиллион",
    "новемвигинтиллион",
    "тригинтиллион",
    "антригинтиллион",
    "дуотригинтиллион"
};

string Rus::Str(string val)
{
    if (val.length() > 100)
        return "Слишком длинное число!";
    
    vector<string> out;
    int len = val.length();
    int stop = len / 3;
    
    if ((len % 3) > 0)
        stop += 1;
    
    
    // Разбиваем число на трехзначные числа (тысячи) и оотдаем на преобразование
    for (int i = 0; i <= stop; ++i)
    {
        string n3;
        
        if ((val.length() - (i * 3 + 3)) < val.length())
            n3 += val[val.length() - (i * 3 + 3)];
        if ((val.length() - (i * 3 + 2)) < val.length())
            n3 += val[val.length() - (i * 3 + 2)];
        if ((val.length() - (i * 3 + 1)) < val.length())
            n3 += val[val.length() - (i * 3 + 1)];
        
        int v = atoi(n3.c_str());
        string str = RusNum::Str(v, (i == 1)?true:false, names[i]);
        out.push_back(str);
        
        
    }
    
    // Собираем пропись большого число из полученных степеней тысячи
    string r;
    for (int j = (out.size() - 1); j >= 0; --j)
    {
        r += out[j];
    }
    
    return r;
    
}

string Rus::DecToOct(string val)
{
    int base = 8;
    string r;
    string tmp;
    int num;
    int d;
    int o;
    int x;
    string value = val;
    stringstream ss;
    string ds;
    
    // Алгоритм последовательного деления "в столбик" на 8
    while(atoi(value.c_str()) != 0)
    {
        ds = "";
        int o = -1;
        int x = 0;
        while (x < value.length())
        {
            if (o < 1)
                tmp = "";
            else
            {
                stringstream sx;
                sx << o;
                tmp = sx.str();
            }
            
            tmp += value[x];
            num = atoi(tmp.c_str());
            
            if (num < base)
            {
                if ((o == 0) && (x < (value.length() - 1)))
                    ds += '0';
                tmp += value[x + 1];
                num = atoi(tmp.c_str());
                x += 2;
            }
            else
            {
                x += 1;
            }
            
            d = num / base;
            o = num % base;
            stringstream ssd;
            ssd << d;
            ds += ssd.str();
        }
        ss << o;	// остаток от деления числа
        
        value = ds;
    }
    
    r = ss.str();
    reverse(r.begin(), r.end());
    return r;
}

void Rus::NumToStr()
{
    string val10;
    string val8;
    
    cout << "Введите число в 10сс:" << endl;
    cin >> val10;
    val8 = Rus::DecToOct(val10);
    cout << "Число в 8сс: " << endl << val8 << endl;
    
    cout << "10cc:\n" << Rus::Str(val10) << endl;
    cout << "8cc:\n" << Rus::Str(val8) << endl;
    
}Konstantin
